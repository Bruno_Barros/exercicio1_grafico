package funcionariobanco;

public class Conta {

    private Pessoa Titular;
    private int numero, digito;
    private float saldo;

    boolean debita(float valor) {
        if (saldo < valor) {
            saldo -= valor;
            return true;
        } else {
            return false;
        }
    }

    void credita(float valor) {
        saldo += valor;
    }

    public void setTitular(Pessoa Titular) {
        this.Titular = Titular;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

}
